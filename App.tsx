import React, { Suspense, useState, useRef, useEffect } from "react";

import { useFonts } from "expo-font";
import AppLoading from "expo-app-loading";

// rn import
import { StyleSheet } from "react-native";

// react-navigation import
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

// Screens
import { Welcome } from "./src/screens/Welcome";

// stack navigator page one over the other
const AuthenticationStack = createStackNavigator();

const AuthenticationNavigator = () => {
	return (
		<AuthenticationStack.Navigator headerMode="none">
			<AuthenticationStack.Screen name="Main" component={Welcome} />
		</AuthenticationStack.Navigator>
	);
};

export default function App() {
	let [fontsLoaded] = useFonts({
		Apollo: require("./assets/fonts/APOLLO.otf"),
		RighteousRegular: require("./assets/fonts/Righteous-Regular.ttf"),
	});
	if (!fontsLoaded) {
		return <AppLoading />;
	} else {
		return (
			// <Suspense fallback="loading">
			<NavigationContainer>
				<AuthenticationNavigator />
			</NavigationContainer>
			// </Suspense>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
	},
});
