import * as React from "react";
import { StyleSheet, Image } from "react-native";

const ImageLogo = ({ team }: { team: string }) => {
	switch (team) {
		case "ATL":
			return <Image source={require(`./../../../assets/teams/ATL.png`)} style={styles.logo} />;
		case "BKN":
			return <Image source={require(`./../../../assets/teams/BKN.png`)} style={styles.logo} />;
		case "BOS":
			return <Image source={require(`./../../../assets/teams/BOS.png`)} style={styles.logo} />;
		case "CHA":
			return <Image source={require(`./../../../assets/teams/CHA.png`)} style={styles.logo} />;
		case "CHI":
			return <Image source={require(`./../../../assets/teams/CHI.png`)} style={styles.logo} />;
		case "CLE":
			return <Image source={require(`./../../../assets/teams/CLE.png`)} style={styles.logo} />;
		case "DAL":
			return <Image source={require(`./../../../assets/teams/DAL.png`)} style={styles.logo} />;
		case "DEN":
			return <Image source={require(`./../../../assets/teams/DEN.png`)} style={styles.logo} />;
		case "DET":
			return <Image source={require(`./../../../assets/teams/DET.png`)} style={styles.logo} />;
		case "GSW":
			return <Image source={require(`./../../../assets/teams/GSW.png`)} style={styles.logo} />;
		case "HOU":
			return <Image source={require(`./../../../assets/teams/HOU.png`)} style={styles.logo} />;
		case "IND":
			return <Image source={require(`./../../../assets/teams/IND.png`)} style={styles.logo} />;
		case "LAC":
			return <Image source={require(`./../../../assets/teams/LAC.png`)} style={styles.logo} />;
		case "LAL":
			return <Image source={require(`./../../../assets/teams/LAL.png`)} style={styles.logo} />;
		case "MEM":
			return <Image source={require(`./../../../assets/teams/MEM.png`)} style={styles.logo} />;
		case "MIA":
			return <Image source={require(`./../../../assets/teams/MIA.png`)} style={styles.logo} />;
		case "MIL":
			return <Image source={require(`./../../../assets/teams/MIL.png`)} style={styles.logo} />;
		case "MIN":
			return <Image source={require(`./../../../assets/teams/MIN.png`)} style={styles.logo} />;
		case "NOP":
			return <Image source={require(`./../../../assets/teams/NOP.png`)} style={styles.logo} />;
		case "NYK":
			return <Image source={require(`./../../../assets/teams/NYK.png`)} style={styles.logo} />;
		case "OKC":
			return <Image source={require(`./../../../assets/teams/OKC.png`)} style={styles.logo} />;
		case "ORL":
			return <Image source={require(`./../../../assets/teams/ORL.png`)} style={styles.logo} />;
		case "PHI":
			return <Image source={require(`./../../../assets/teams/PHI.png`)} style={styles.logo} />;
		case "PHX":
			return <Image source={require(`./../../../assets/teams/PHX.png`)} style={styles.logo} />;
		case "POR":
			return <Image source={require(`./../../../assets/teams/POR.png`)} style={styles.logo} />;
		case "SAC":
			return <Image source={require(`./../../../assets/teams/SAC.png`)} style={styles.logo} />;
		case "SAS":
			return <Image source={require(`./../../../assets/teams/SAS.png`)} style={styles.logo} />;
		case "TOR":
			return <Image source={require(`./../../../assets/teams/TOR.png`)} style={styles.logo} />;
		case "UTA":
			return <Image source={require(`./../../../assets/teams/UTA.png`)} style={styles.logo} />;
		case "WAS":
			return <Image source={require(`./../../../assets/teams/WAS.png`)} style={styles.logo} />;
		default:
			return <Image source={require(`./../../../assets/teams/ATL.png`)} style={styles.logo} />;
	}
};

export default ImageLogo;

const styles = StyleSheet.create({
	logo: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 50,
		width: 50,
		height: 50,
	},
});
