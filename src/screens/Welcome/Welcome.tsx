import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, ActivityIndicator, ScrollView, SafeAreaView, RefreshControl } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import GestureRecognizer from "react-native-swipe-gestures";

import axios from "axios";
import ImageLogo from "../../modules/ImageLogo/ImageLogo";

const WelcomeComponent = ({ navigation }: { navigation: any }) => {
	const [isLoading, setIsLoading] = useState(false);
	const [todayNBAGames, setTodayNBAGames] = useState([]);
	const [dateSelected, setDateSelected] = useState(new Date());
	const [refreshing, setRefreshing] = React.useState(false);

	const config = {
		velocityThreshold: 0.3,
		directionalOffsetThreshold: 80,
	};

	useEffect(() => {
		getTodaysNBAGame();
	}, []);

	useEffect(() => {
		getTodaysNBAGame();
	}, [dateSelected]);

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		getTodaysNBAGame();
	}, [dateSelected]);

	const getTodaysNBAGame = () => {
		const year = dateSelected.getFullYear();
		const month = dateSelected.getMonth() + 1;
		const day = dateSelected.getDate();
		setIsLoading(true);

		axios
			.get(`https://www.balldontlie.io/api/v1/games?start_date=${year}-${month}-${day}&end_date=${year}-${month}-${day}`)
			.then((response: any) => {
				setIsLoading(false);
				setRefreshing(false);
				setTodayNBAGames(response.data.data);
			})
			.catch((error: any) => {
				console.log("error: ", error);
				setIsLoading(false);
			});
	};

	const isWinner = (score1: number, score2: number) => {
		return score1 > score2;
	};

	const showNBATodaysGame = () => {
		const nbaGames = todayNBAGames.map((game: any, index: number) => {
			return (
				<View style={styles.nbaCard} key={index}>
					<View style={styles.nbaTimeCard}>
						<Text style={styles.nbaCardText}>{game.status}</Text>
						{game.time !== "" && <Text style={styles.nbaCardTextTime}>{game.time}</Text>}
					</View>
					<View style={styles.cardLogoTeamContainer}>
						<ImageLogo team={game.home_team.abbreviation} />
						<ImageLogo team={game.visitor_team.abbreviation} />
					</View>
					<View style={styles.nbaScoreCard}>
						<Text style={styles.nbaCardText}>{game.home_team.abbreviation}</Text>
						<Text style={[styles.nbaCardText, { color: isWinner(game.home_team_score, game.visitor_team_score) ? "#3ab686" : "#525156" }]}>{game.home_team_score}</Text>
						<Text style={styles.nbaCardText}> - </Text>
						<Text style={[styles.nbaCardText, { color: isWinner(game.visitor_team_score, game.home_team_score) ? "#3ab686" : "#525156" }]}>{game.visitor_team_score}</Text>
						<Text style={styles.nbaCardText}>{game.visitor_team.abbreviation}</Text>
					</View>
				</View>
				// <LinearGradient colors={["#1847a6", "#3f5efb", "#1847a6"]} start={{ x: 0.0, y: 0 }} end={{ x: 1, y: 0 }} style={styles.nbaCard} key={index}>
				// 	<View style={styles.nbaTimeCard}>
				// 		<Text style={styles.nbaCardText}>{game.status}</Text>
				// 		{game.time !== "" && <Text style={styles.nbaCardText}>{game.time}</Text>}
				// 	</View>
				// 	<View style={styles.nbaScoreCard}>
				// 		<Text style={styles.nbaCardText}>{game.home_team.abbreviation}</Text>
				// 		<Text style={styles.nbaCardText}>{game.home_team_score}</Text>
				// 		<Text style={styles.nbaCardText}> - </Text>
				// 		<Text style={styles.nbaCardText}>{game.visitor_team_score}</Text>
				// 		<Text style={styles.nbaCardText}>{game.visitor_team.abbreviation}</Text>
				// 	</View>
				// </LinearGradient>
			);
		});

		return <View style={styles.nbaGameCardsContainer}>{nbaGames}</View>;
	};

	return (
		<SafeAreaView style={styles.container}>
			{/* <LinearGradient colors={["#1847a6", "#ff1f3c"]} start={{ x: 0.0, y: 1 }} end={{ x: 0, y: 0 }}> */}
			<View style={styles.headerDate}>
				<Text style={styles.headerDateText}>{dateSelected.toDateString()}</Text>
			</View>
			{!isLoading && todayNBAGames ? (
				<ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
					<GestureRecognizer onSwipeLeft={(state) => setDateSelected(new Date(dateSelected.setDate(dateSelected.getDate() + 1)))} onSwipeRight={(state) => setDateSelected(new Date(dateSelected.setDate(dateSelected.getDate() - 1)))} config={config}>
						{showNBATodaysGame()}
					</GestureRecognizer>
				</ScrollView>
			) : (
				<ActivityIndicator size="large" color="#cccccc" />
			)}
			{/* </LinearGradient> */}
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fafafa",
		paddingTop: 50,
	},
	headerDate: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: 50,
	},
	headerDateText: {
		color: "black",
		fontSize: 24,
		fontFamily: "RighteousRegular",
		fontWeight: "800",
	},
	nbaGameCardsContainer: {
		display: "flex",
		alignItems: "center",
		width: "100%",
	},
	nbaCard: {
		display: "flex",
		flexDirection: "column",
		width: "90%",
		height: 120,
		justifyContent: "space-around",
		alignItems: "center",
		borderRadius: 10,
		marginTop: 10,
		backgroundColor: "white",
	},
	cardLogoTeamContainer: {
		display: "flex",
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-around",
	},
	nbaScoreCard: {
		display: "flex",
		flexDirection: "row",
		width: "100%",
		height: "60%",
		justifyContent: "space-around",
		alignItems: "center",
	},
	nbaTimeCard: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		height: "40%",
		justifyContent: "center",
		alignItems: "center",
	},
	nbaCardText: {
		color: "#525156",
		fontSize: 20,
		fontFamily: "RighteousRegular",
		fontWeight: "800",
	},
	nbaCardTextTime: {
		color: "#e1323e",
		fontSize: 18,
		fontFamily: "RighteousRegular",
		fontWeight: "800",
	},
});

export default WelcomeComponent;
